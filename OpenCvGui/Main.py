'''
Created on 22 sie 2016

@author: euroavionic
'''

from PySide.QtCore import *
from PySide.QtGui import *
import sys
import cv2
import ImageQt
import Funkcje
import Gui

import cv2




class MainWindow(QMainWindow,Gui.Ui_MainWindow):
    
    def __init__(self,parent = None):
        super(MainWindow, self).__init__(parent)
        self.video_size = QSize(320, 240)
        self.setupUi(self)
        #self.setup_camera()
        
        self.pushButton.clicked.connect(self.do_test)
          
      
        self.setup_camera()
        
        
       
    
    def setup_camera(self):
        """Initialize camera.
        """
        #=======================================================================
        # self.capture = cv2.VideoCapture(0)
        # self.capture.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, self.video_size.width())
        # self.capture.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, self.video_size.height())
        #=======================================================================
        
        self.capture2 = cv2.VideoCapture(1)
        self.capture2.set(cv2.cv.CV_CAP_PROP_FRAME_WIDTH, self.video_size.width())
        self.capture2.set(cv2.cv.CV_CAP_PROP_FRAME_HEIGHT, self.video_size.height())
        
        
        
    def do_test(self):
        self.display_image()
    
        
    def display_image(self):
          
        while True:
           
            cv2.waitKey(1)
            _2, frame2 = self.capture2.read()
            frame2 = cv2.cvtColor(frame2, cv2.cv.CV_BGR2RGB)
            frame2 = cv2.flip(frame2, 1)
            image2 = QImage(frame2, frame2.shape[1], frame2.shape[0],frame2.strides[0], QImage.Format_RGB888)
            self.label_2.setPixmap(QPixmap.fromImage(image2))
             
                              
        

    
    def Topic_state(self):
        self.label_3.setText("lol")

     
         

app = QApplication(sys.argv)
form = MainWindow()
form.show()
app.exec_()
